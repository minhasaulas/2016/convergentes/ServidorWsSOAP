package br.up.edu;

import javax.jws.WebService;

@WebService(endpointInterface="br.up.edu.WS")
public class WSImpl implements WS {

	@Override
	public double somar(double a, double b) {
		return a + b;
	}
}